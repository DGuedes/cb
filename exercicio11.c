#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

int main(){
    int numero[500];
    int j, i;
    bool repetido = false;

    for(i=0;i<500;i++){
        scanf("%d", &numero[i]);
        for(j=0;j<i;j++){
            if(numero[j] == numero[i]){
                repetido = true;
            }
        }
        if(repetido == false){
            printf("NOVO NUMERO: %d\n", numero[i]);
        }
        repetido = false;
    }
    return 0;
}
