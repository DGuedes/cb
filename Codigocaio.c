/*

    Construa um algoritmo que calcule o novo sal�rio (SAL_NOVO) para cada um dos funcion�rios da
empresa. Considere que o funcion�rio dever� receber um reajuste de 15% caso seu sal�rio (SAL) seja
menor que 500. Se o sal�rio for maior ou igual a 500, mas menor ou igual a 1000, o reajuste deve ser de
10%.Caso o sal�rio sejamaior que 1000, o reajuste deve ser de 5%.O programa deve parar quando for
digitado um sal�rio (SAL) com valor negativo, ou seja,inv�lido.Al�m disso, ao final, o programa deve
apresentar quanto ser� gasto a mais pela empresa com esses aumentos.
*/

#include <stdlib.h>
#include <stdio.h>

main(){

    float despesa=0, salario=0, new_salario=0;

    scanf("%f", &salario);
    while(salario<0){
        printf("ERRO! Valor invalido. Entre com um novo valor.\n");
        scanf("%f, &salario");
    }

    if(salario < 500){
        new_salario = salario*1.15;
        despesa = despesa+(new_salario - salario);
    }
    if((salario>500 || salario==500) && (salario<1000 || salario==1000)){
        new_salario = salario*1.1;
        despesa = despesa+(new_salario - salario);
    }
    if(salario>1000){
        new_salario = salario*1.05;
        despesa = despesa+(new_salario - salario);
    }

    printf("Salario digitado: %f\n", salario);
    printf("Novo salario: %f\n", new_salario);
    printf("Despesas adicionais da empresa: %f\n", despesa);
    system("PAUSE");
}
