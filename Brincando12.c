#include <stdio.h>
#include <stdlib.h>

int main(){

    int he, me, hs, ms, contador=0, pagamento, i;
    int tempo_total;

    do{
        printf("digite a hora de entrada\n");
        scanf("%d", &he);
    }while(he<0 && he>23);

    do{
        printf("digite o minuto de entrada\n");
        scanf("%d", &me);
    }while(me<0 && me>59);

    do{
        printf("digite a hora de saida\n");
        scanf("%d", &hs);
    }while(hs<0 && hs>23);

    do{
        printf("digite o minuto de saida\n");
        scanf("%d", &ms);
    }while(ms<0 && ms>59);

    tempo_total =(hs-he)*60+(ms-me);

    if(tempo_total<=60){
        pagamento = 4;
        printf("o valor que devera ser pago eh de: %d", pagamento);
    }

    if(tempo_total>60 && tempo_total<=120){
        pagamento = 6;
        printf("o valor que devera ser pago eh de: %d", pagamento);
    }

    if(tempo_total>120){
        for(i=121; i<=tempo_total; i=i+60){
        contador++;
        pagamento = 6+(1*contador);
        }
    printf("o valor que devera ser pago eh de: %d", pagamento);
    }

    return 0;
}
